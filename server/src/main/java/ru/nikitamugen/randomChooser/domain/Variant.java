package ru.nikitamugen.randomChooser.domain;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "VARIANT")
public class Variant implements Serializable {

    private static long serialVersionUID = 1;

    @Id
    @Column(name = "VARIANT_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "variant_seq_gen")
    @SequenceGenerator(name = "variant_seq_gen", sequenceName = "variant_id_seq")
    @JsonProperty(defaultValue = "0")
    public Long id = 0L;

    @Column(name = "VARIANT_NAME", nullable = false)
    @JsonProperty(required = true)
    public String name;

    @Basic
    @Column(name = "VARIANT_IS_CHOSEN", columnDefinition = "BOOL")
    public Boolean isChosen;

    @Basic
    @Column(name = "VARIANT_IS_CURRENT", columnDefinition = "BOOL")
    public Boolean isCurrent;

    @ManyToOne
    @JoinColumn(name = "VARIANT_LIST_NAME")
    @JsonIgnore
    public VariantList variantList;

    @Transient
    @JsonProperty(required = true)
    public String variantListName;

    @JsonGetter(value = "variantListName")
    public String getVariantListName() {
        return this.variantList.name;
    }
}
