package ru.nikitamugen.randomChooser.domain;

import java.io.Serializable;
import java.util.List;

public class SlackMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    public String token;
    public String channel;
    public String text;
    public SlackMessageParse parse = SlackMessageParse.full;
    public Boolean link_names = true;
    public List<SlackAttachment> attachments;
    public Boolean unfurl_links = true;
    public Boolean unfurl_media = false;
    public String username;
    public Boolean as_user = false;
    public String icon_url;
    public String icon_emoji;
    public String thread_ts;
    public Boolean reply_broadcast = true;

    public String toRequestString() {
        final String delimiter = "&";
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("token=").append(token).append(delimiter);
        stringBuilder.append("text=").append(text).append(delimiter);
        stringBuilder.append("channel=").append(channel);

        return stringBuilder.toString();
    }
}
