package ru.nikitamugen.randomChooser.domain;

public class EventMessage {

    private String text;

    public String getText() {
        return text;
    }

    private String listName;

    public String getListName() {
        return listName;
    }

    private EventMessageType type;

    public EventMessageType getType() {
        return type;
    }

    private String clientGuid;

    public String getClientGuid() {
        return clientGuid;
    }

    private EventMessage(EventMessageBuilder builder) {
        this.text = builder.getText();
        this.listName = builder.getListName();
        this.type = builder.getType();
        this.clientGuid = builder.getClientGuid();
    }

    @Override
    public String toString() {
        final String format = "{\"text\": \"%s\", \"listName\": \"%s\", \"type\": \"%s\"}";
        return String.format(format, text, listName, type);
    }

    public static class EventMessageBuilder {
        private String text;
        private String listName;
        private EventMessageType type;
        private String clientGuid;

        public EventMessageBuilder () {}
        public EventMessage build() {
            return new EventMessage(this);
        }

        public String getText() {
            return text;
        }

        public String getListName() {
            return listName;
        }

        public EventMessageType getType() {
            return type;
        }

        public String getClientGuid() {
            return clientGuid;
        }

        public EventMessageBuilder text(String value) {
            this.text = value;
            return this;
        }

        public EventMessageBuilder listName(String value) {
            this.listName = value;
            return this;
        }

        public EventMessageBuilder type(EventMessageType value) {
            this.type = value;
            return this;
        }

        public EventMessageBuilder clientGuid(String value) {
            this.clientGuid = value;
            return this;
        }
    }
}
