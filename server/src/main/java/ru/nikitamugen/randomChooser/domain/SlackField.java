package ru.nikitamugen.randomChooser.domain;

import java.io.Serializable;

public class SlackField implements Serializable {
    private static final long serialVersionUID = 1L;

    public String title;

    public String value;

    public Boolean isShort; // short - not allowed
}
