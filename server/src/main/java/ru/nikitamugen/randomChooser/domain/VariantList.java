package ru.nikitamugen.randomChooser.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "VARIANT_LIST", uniqueConstraints = {
        @UniqueConstraint(columnNames = "VARIANT_LIST_NAME")
})
public class VariantList implements Serializable {

    private static long serialVersionUID = 1;

    @Id
    @Column(name = "VARIANT_LIST_NAME", nullable = false, unique = true)
    @JsonProperty(required = true)
    public String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, mappedBy = "variantList")
    @JsonIgnore
    public Set<Variant> variants;

    @Basic
    @Column(name = "VARIANT_LIST_SEND_CHOSEN_NOTIFIER", columnDefinition = "BOOL")
    public Boolean sendChosenChangeNotifier;

    @Column(name = "VARIANT_LIST_SEND_CHOSEN_NOTIFIER_TEMPLATE")
    public String sendChosenNotifierTemplate;

    @Column(name = "VARIANT_LIST_SLACK_CHANNEL")
    public String slackChannel;
}
