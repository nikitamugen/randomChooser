package ru.nikitamugen.randomChooser.domain;

import java.io.Serializable;
import java.util.List;

public class SlackAttachment implements Serializable {
    private static final long serialVersionUID = 1L;

    public String fallback;
    public String color;
    public String pretext;
    public String author_name;
    public String author_link;
    public String author_icon;
    public String title;
    public String title_link;
    public String text;
    public List<SlackField> fields;
    public String image_url;
    public String thumb_url;
    public String footer;
    public String footer_icon;
    public String ts;
}
