package ru.nikitamugen.randomChooser.domain;

public enum EventMessageType  {
    CHANGE, VARIANT_LIST_REMOVE, CONNECTED
}
