package ru.nikitamugen.randomChooser.dao.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nikitamugen.randomChooser.domain.Variant;
import ru.nikitamugen.randomChooser.domain.VariantList;

import java.util.List;

@Repository
public interface VariantRepository extends CrudRepository<Variant, Long> {
    List findAll();

    List findAllByVariantListOrderById(VariantList variantList);

    List findAllByVariantListAndIsChosenOrderById(VariantList variantList, boolean isChosen);
}
