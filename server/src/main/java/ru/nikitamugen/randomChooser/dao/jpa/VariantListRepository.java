package ru.nikitamugen.randomChooser.dao.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nikitamugen.randomChooser.domain.VariantList;

import java.util.List;

@Repository
public interface VariantListRepository extends CrudRepository<VariantList, String> {
    List findAll();

    VariantList getByName(String name);
}
