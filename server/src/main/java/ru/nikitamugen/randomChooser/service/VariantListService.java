package ru.nikitamugen.randomChooser.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.nikitamugen.randomChooser.api.rest.SSEController;
import ru.nikitamugen.randomChooser.dao.jpa.VariantListRepository;
import ru.nikitamugen.randomChooser.dao.jpa.VariantRepository;
import ru.nikitamugen.randomChooser.domain.SlackMessage;
import ru.nikitamugen.randomChooser.domain.Variant;
import ru.nikitamugen.randomChooser.domain.VariantList;

import java.util.List;
import java.util.ListIterator;
import java.util.Random;

@Service
public class VariantListService {

    @Autowired
    private VariantListRepository variantListRepository;

    @Autowired
    private VariantRepository variantRepository;

    @Autowired
    private VariantService variantService;

    @Autowired
    private SlackService slackService;

    @Autowired
    private SSEController sseController;

    private Logger logger = LoggerFactory.getLogger(VariantListService.class);

    public List<VariantList> getAll() {
        return variantListRepository.findAll();
    }

    public VariantList getByName(String name) {
        return variantListRepository.findOne(name);
    }

    public VariantList saveVariantList(VariantList variantList) {
        VariantList newVariantList = variantListRepository.save(variantList);

        final String message = "Variant list changed";
        sseController.dispatchChange(newVariantList.name, message);

        return newVariantList;
    }

    public void deleteVariantList(VariantList variantList) {
        variantListRepository.delete(variantList);

        final String message = "Variant list removed";
        sseController.dispatchVariantListRemoved(variantList.name, message);
    }

    public void clearResults(VariantList variantList) {
        Assert.notNull(variantList, "Variant list is null");

        List<Variant> allVariants = variantRepository.findAllByVariantListOrderById(variantList);
        Assert.isTrue(!allVariants.isEmpty(), "Variant list is empty. Nothing to change.");

        ListIterator<Variant> variantListIterator = allVariants.listIterator();
        while (variantListIterator.hasNext()) {
            Variant variant = variantListIterator.next();
            variant.isChosen = false;
            variantRepository.save(variant);
        }

        final String message = "All results cleared";
        sseController.dispatchChange(variantList.name, message);
    }

    public void clearVariants(VariantList variantList) {
        Assert.notNull(variantList, "Variant list is null");

        List<Variant> allVariants = variantRepository.findAllByVariantListOrderById(variantList);
        Assert.isTrue(!allVariants.isEmpty(), "Variant list is empty. Nothing to remove.");
        variantRepository.delete(allVariants);

        final String message = "All variants cleared";
        sseController.dispatchChange(variantList.name, message);
    }

    private List<Variant> getNotChosenVariants(VariantList variantList) {
        Assert.notNull(variantList, "Variant list is null");
        return variantRepository.findAllByVariantListAndIsChosenOrderById(variantList, false);
    }

    public Variant chooseNextVariant(VariantList variantList) {
        List<Variant> notChosenList = getNotChosenVariants(variantList);
        if (notChosenList.isEmpty()) {
            clearResults(variantList);
            notChosenList = getNotChosenVariants(variantList);
        }

        // next item - always first from not chosen
        //
        final int nextIndex = 0;
        Variant nextVariant = notChosenList.get(nextIndex);
        Assert.notNull(nextVariant, "Next variant is null. Something goes seriously wrong ...");

        if (variantList.sendChosenChangeNotifier) {
            sendChangeMessage(variantList, nextVariant);
        }

        return variantService.toggleVariant(nextVariant);
    }

    public Variant chooseRandomVariant(VariantList variantList) {
        List<Variant> notChosenList = getNotChosenVariants(variantList);
        if (notChosenList.isEmpty()) {
            clearResults(variantList);
            notChosenList = getNotChosenVariants(variantList);
        }

        final Random random = new Random(System.currentTimeMillis());
        final int randomBound = notChosenList.size();
        int nextIndex = 0;
        if (randomBound > 0) {
            nextIndex = random.nextInt(randomBound);
        }
        Variant nextVariant = notChosenList.get(nextIndex);
        Assert.notNull(nextVariant, "Next variant is null. Something goes seriously wrong ...");

        if (variantList.sendChosenChangeNotifier) {
            sendChangeMessage(variantList, nextVariant);
        }

        return variantService.toggleVariant(nextVariant);
    }

    private void sendChangeMessage(VariantList variantList, Variant variant) {
        Assert.notNull(variantList, "Variant list param is empty");
        Assert.notNull(variant, "Variant param is empty");

        SlackMessage slackMessage = new SlackMessage();
        slackMessage.text = variantList.sendChosenNotifierTemplate + variant.name;
        slackMessage.channel = variantList.slackChannel;
        slackService.sendMessage(slackMessage);
    }
}
