package ru.nikitamugen.randomChooser.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import ru.nikitamugen.randomChooser.domain.SlackMessage;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Service
public class SlackService {

    private static final String codePage = "UTF-8";
    private static final String slackPostMethodURI = "https://slack.com/api/chat.postMessage";

    @Value("${slack.token}")
    private String slackToken;

    @Value("${slack.mainChannel}")
    private String mainChannel;

    private Logger logger = LoggerFactory.getLogger(SlackService.class);

    public void sendMessage(SlackMessage slackMessage) {
        Assert.notNull(slackMessage, "Slack message is null");
        if (StringUtils.isEmpty(slackMessage.token)) {
            slackMessage.token = slackToken;
        }
        if (StringUtils.isEmpty(slackMessage.channel)) {
            slackMessage.channel = mainChannel;
        }
        if (!StringUtils.isEmpty(slackMessage.text)) {
            slackMessage.text = encodeBody(slackMessage.text);
        }

        try {
            final String slackMessageString = slackMessage.toRequestString();

            final StringBuilder stringURIBuilder = new StringBuilder(slackPostMethodURI);
            stringURIBuilder.append("?").append(slackMessageString);
            logger.warn(stringURIBuilder.toString());

            HttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost(stringURIBuilder.toString());
            httppost.setHeader("Content-Type", "text/plain");

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream inputStream = entity.getContent();
                try {
                    String payloadString = IOUtils.toString(inputStream, codePage);
                    logger.warn(payloadString);
                } finally {
                    inputStream.close();
                }
            } else {
                logger.warn("no response !!!");
            }

        } catch (JsonProcessingException ex) {
            logger.error(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private String encodeBody(String body) {
        String encodedBody = "";
        try {
            encodedBody = URLEncoder.encode(body, codePage);
        } catch (UnsupportedEncodingException ex) {
            logger.error(ex.getMessage(), ex);
        }
        return encodedBody;
    }
}
