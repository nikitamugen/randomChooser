package ru.nikitamugen.randomChooser.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.nikitamugen.randomChooser.api.rest.SSEController;
import ru.nikitamugen.randomChooser.dao.jpa.VariantListRepository;
import ru.nikitamugen.randomChooser.dao.jpa.VariantRepository;
import ru.nikitamugen.randomChooser.domain.Variant;
import ru.nikitamugen.randomChooser.domain.VariantList;

import java.util.List;

@Service
public class VariantService {

    private static Logger logger = LoggerFactory.getLogger(VariantService.class);

    @Autowired
    private VariantRepository variantRepository;

    @Autowired
    private VariantListRepository variantListRepository;

    @Autowired
    private SSEController sseController;

    public List<Variant> getAll() {
        return variantRepository.findAll();
    }

    public Variant findById(Long id) {
        Assert.isTrue(id > 0, "Variant id should be not empty");
        return variantRepository.findOne(id);
    }

    public List<Variant> getAllVariantsByVariantListName(String variantListName) {
        Assert.hasText(variantListName, "Variant list name should be not empty");

        VariantList variantList = variantListRepository.getByName(variantListName);
        return variantRepository.findAllByVariantListOrderById(variantList);
    }

    public Variant saveVariant(Variant variant) {
        Assert.notNull(variant, "Variant should be not null");
        Assert.hasText(variant.variantListName, "variantListName should be not empty");

        VariantList existsVariantList = variantListRepository.getByName(variant.variantListName);
        variant.variantList = existsVariantList;

        StringBuilder messageBuilder = new StringBuilder();
        if (variant.id > 0) {
            Variant oldVariant = variantRepository.findOne(variant.id);
            messageBuilder.append("Variant changed: ");
        } else {
            messageBuilder.append("Created new variant: ");
        }

        Variant newVariant = variantRepository.save(variant);

        messageBuilder.append(newVariant.name);
        sseController.dispatchChange(existsVariantList.name, messageBuilder.toString());

        return newVariant;
    }

    public void deleteVariant(Variant variant) {
        Assert.notNull(variant, "Variant should be not null");

        final String variantListName = variant.getVariantListName();
        final String variantName = variant.name;
        variantRepository.delete(variant);

        StringBuilder messageBuilder = new StringBuilder("Variant removed ");
        messageBuilder.append(variantName);
        sseController.dispatchChange(variantListName, messageBuilder.toString());
    }

    public Variant toggleVariant(Variant variant) {
        Assert.notNull(variant, "Variant is null");
        variant.isChosen = !variant.isChosen;
        Variant newVariant = variantRepository.save(variant);

        StringBuilder messageBuilder = new StringBuilder(variant.name);
        if (newVariant.isChosen) {
            messageBuilder.append(" choosed");
        } else {
            messageBuilder.append(" unchoosed");
        }
        sseController.dispatchChange(newVariant.getVariantListName(), messageBuilder.toString());

        return newVariant;
    }

}
