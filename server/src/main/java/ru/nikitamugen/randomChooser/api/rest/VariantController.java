package ru.nikitamugen.randomChooser.api.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.nikitamugen.randomChooser.domain.Variant;
import ru.nikitamugen.randomChooser.service.VariantService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value = "api/v1/variant")
@Api(tags = {"variant"})
public class VariantController extends AbstractRestHandler {

    @Autowired
    VariantService variantService;

    private Logger logger = LoggerFactory.getLogger(VariantController.class);

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<Variant> getAll(HttpServletRequest request, HttpServletResponse response) {
        return variantService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    Variant getById(@ApiParam(value = "The id of the existing variant resource.", required = true)
                    @PathVariable("id") Long id,
                    HttpServletRequest request, HttpServletResponse response) {
        Variant existsVariant = variantService.findById(id);
        checkResourceFound(existsVariant);
        return existsVariant;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/variantList/{variantListName}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<Variant> getAllByVariantListName(@ApiParam(value = "The name of the existing variant list resource.", required = true)
                                          @PathVariable("variantListName") String variantListName,
                                          HttpServletRequest request, HttpServletResponse response) {
        return variantService.getAllVariantsByVariantListName(variantListName);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "",
            method = RequestMethod.POST,
            consumes = {"application/json"},
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Save a variant resource.", notes = "Returns the URL of the resource in the Location header.")
    public void saveVariant(@RequestBody Variant variant,
                            HttpServletRequest request, HttpServletResponse response) {
        Variant savedVariant = variantService.saveVariant(variant);
        checkResourceFound(savedVariant);
        response.setHeader("Location", request.getRequestURL().append("/").append(savedVariant.id).toString());
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete a variant resource.")
    public void deleteVariant(@ApiParam(value = "The id of the existing variant list resource.", required = true)
                              @PathVariable("id") Long id,
                              HttpServletRequest request, HttpServletResponse response) {
        Variant variant = variantService.findById(id);

        checkResourceFound(variant);
        variantService.deleteVariant(variant);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/toggle/{id}",
            method = RequestMethod.POST,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Toggle choosed state for a variant resource.")
    public void toggleVariant(@ApiParam(value = "The id of the existing variant resource.", required = true)
                              @PathVariable("id") Long id,
                              HttpServletRequest request, HttpServletResponse response) {
        Variant variant = variantService.findById(id);
        checkResourceFound(variant);

        variantService.toggleVariant(variant);
    }
}
