package ru.nikitamugen.randomChooser.api.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import ru.nikitamugen.randomChooser.domain.EventMessage;
import ru.nikitamugen.randomChooser.domain.EventMessageType;
import ru.nikitamugen.randomChooser.domain.VariantList;
import ru.nikitamugen.randomChooser.service.VariantListService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping(value = "api/v1/events")
@Api(tags = {"events"})
@Service
public class SSEController extends AbstractRestHandler {

    @Autowired
    private VariantListService variantListService;

    private static Logger logger = LoggerFactory.getLogger(SSEController.class);

    private Map<String, Map<String, SseEmitter>> sseEmitterMap =
            new ConcurrentHashMap<>();

    private final String emptyListNameText =
            "List name param is empty";

    public void dispatchChange(String listName, String message) {
        Assert.hasLength(listName, emptyListNameText);
        EventMessage.EventMessageBuilder eventMessageBuilder =
                new EventMessage.EventMessageBuilder();
        eventMessageBuilder
                .text(message)
                .listName(listName)
                .type(EventMessageType.CHANGE);

        dispatchEventMessage(eventMessageBuilder.build());
    }

    public void dispatchVariantListRemoved(String listName, String message) {
        Assert.hasLength(listName, emptyListNameText);
        EventMessage.EventMessageBuilder eventMessageBuilder =
                new EventMessage.EventMessageBuilder();
        eventMessageBuilder
                .text(message)
                .listName(listName)
                .type(EventMessageType.VARIANT_LIST_REMOVE);

        dispatchEventMessage(eventMessageBuilder.build());
    }

    private void dispatchEventMessage(EventMessage eventMessage) {
        Assert.notNull(eventMessage, "Event message is null");
        sseEmitterMap.forEach((listName, clientMap) -> {
            if (listName.equals(eventMessage.getListName())) {
                clientMap.forEach((clientGuid, sseEmitter) -> {
                    try {
                        sseEmitter.send(eventMessage.toString());
                    } catch (IOException ex) {
                        logger.error("Client "+clientGuid+" is logoff. Remove");
                        sseEmitter.complete();
                        clientMap.remove(clientGuid);
                    }
                    catch (IllegalStateException ex) {
                        logger.error("Client "+clientGuid+" is complete. Remove");
                        clientMap.remove(clientGuid);
                    }
                });
            }
        });
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/{listName}/{clientGuid}")
    public
    @ResponseBody
    SseEmitter addListenerByListName(
            @ApiParam(value = "The id of the existing variant resource.", required = true)
            @PathVariable("listName") String listName,
            @PathVariable("clientGuid") String clientGuid,
            HttpServletRequest request, HttpServletResponse response) {

        Assert.hasLength(listName, emptyListNameText);

        VariantList variantList = variantListService.getByName(listName);
        checkResourceFound(variantList);

        final Long requestLive = 60 * 1000L;
        SseEmitter sseEmitter = new SseEmitter(requestLive);

        try {
            EventMessage.EventMessageBuilder eventMessageBuilder =
                    new EventMessage.EventMessageBuilder();
            eventMessageBuilder
                    .text("connected")
                    .listName(listName)
                    .type(EventMessageType.CONNECTED)
                    .clientGuid(clientGuid);

            sseEmitter.send(eventMessageBuilder.build().toString());

            Map<String, SseEmitter> currentListEmitterMap =
                    sseEmitterMap.getOrDefault(listName, new ConcurrentHashMap<String, SseEmitter>());
            currentListEmitterMap.put(clientGuid, sseEmitter);
            sseEmitterMap.put(listName, currentListEmitterMap);
        } catch (IOException ex) {
            logger.error("Client "+clientGuid+" is already gone.");
            sseEmitter.complete();
        }

        return sseEmitter;
    }
}
