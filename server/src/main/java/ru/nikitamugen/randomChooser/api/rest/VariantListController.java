package ru.nikitamugen.randomChooser.api.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.nikitamugen.randomChooser.domain.Variant;
import ru.nikitamugen.randomChooser.domain.VariantList;
import ru.nikitamugen.randomChooser.service.VariantListService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value = "api/v1/variantList")
@Api(tags = {"variantList"})
public class VariantListController extends AbstractRestHandler {

    @Autowired
    private VariantListService variantListService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<VariantList> getAll(HttpServletRequest request, HttpServletResponse response) {
        return variantListService.getAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{name}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    VariantList getByName(@ApiParam(value = "The name of the existing variant list resource.", required = true)
                          @PathVariable("name") String name,
                          HttpServletRequest request, HttpServletResponse response) {
        VariantList variantList = variantListService.getByName(name);
        checkResourceFound(variantList);
        return variantList;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "",
            method = RequestMethod.POST,
            consumes = {"application/json"},
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Save a variant list resource.", notes = "Returns the URL of the resource in the Location header.")
    public void saveVariantList(@RequestBody VariantList variantList,
                                HttpServletRequest request, HttpServletResponse response) {
        VariantList newVariantList = variantListService.saveVariantList(variantList);
        checkResourceFound(newVariantList);
        response.setHeader("Location", request.getRequestURL().append("/").append(newVariantList.name).toString());
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{name}",
            method = RequestMethod.DELETE,
            consumes = {"application/json"},
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete a variant list resource.")
    public void deleteVariantList(@ApiParam(value = "The name of the existing variant list resource.", required = true)
                                  @PathVariable("name") String name,
                                  HttpServletRequest request, HttpServletResponse response) {
        VariantList variantList = variantListService.getByName(name);
        checkResourceFound(variantList);
        variantListService.deleteVariantList(variantList);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "next/{name}",
            method = RequestMethod.POST,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public void chooseNextVariant(@ApiParam(value = "The name of the existing variant list resource.", required = true)
                                  @PathVariable("name") String name,
                                  HttpServletRequest request, HttpServletResponse response) {
        VariantList variantList = variantListService.getByName(name);
        checkResourceFound(variantList);
        Variant nextVariant = variantListService.chooseNextVariant(variantList);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "randomNext/{name}",
            method = RequestMethod.POST,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public void chooseRandomVariant(@ApiParam(value = "The name of the existing variant list resource.", required = true)
                           @PathVariable("name") String name,
                           HttpServletRequest request, HttpServletResponse response) {
        VariantList variantList = variantListService.getByName(name);
        checkResourceFound(variantList);
        Variant nextVariant = variantListService.chooseRandomVariant(variantList);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/clearResults/{name}",
            method = RequestMethod.POST,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public void clearResults(@ApiParam(value = "The name of the existing variant list resource.", required = true)
                                    @PathVariable("name") String name,
                                    HttpServletRequest request, HttpServletResponse response) {
        VariantList variantList = variantListService.getByName(name);
        checkResourceFound(variantList);
        variantListService.clearResults(variantList);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/clearVariants/{name}",
            method = RequestMethod.POST,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public void clearVariants(@ApiParam(value = "The name of the existing variant list resource.", required = true)
                             @PathVariable("name") String name,
                             HttpServletRequest request, HttpServletResponse response) {
        VariantList variantList = variantListService.getByName(name);
        checkResourceFound(variantList);
        variantListService.clearVariants(variantList);
    }
}
