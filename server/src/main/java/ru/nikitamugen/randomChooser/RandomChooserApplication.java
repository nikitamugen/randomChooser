package ru.nikitamugen.randomChooser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "ru.nikitamugen.randomChooser")
@EnableJpaRepositories(basePackages = {
        "ru.nikitamugen.randomChooser.dao.jpa"})
public class RandomChooserApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(RandomChooserApplication.class, args);
    }
}
