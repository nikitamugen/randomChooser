import Vue from 'vue'
import App from './App'

import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.http.options.crossOrigin = true
Vue.http.headers.common['Access-Control-Allow-Origin'] = __API__
Vue.http.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
Vue.http.headers.common['Accept'] = 'application/json, text/plain, */*'
Vue.http.headers.common['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin'

import Toasted from 'vue-toasted';
Vue.use(Toasted);

import VueRouter from 'vue-router'
Vue.use(VueRouter)

Vue.config.productionTip = false
Vue.directive('focus', {
	inserted: function (el) {
		el.focus()
	}
})
const router = new VueRouter({
  transitionOnLoad: true,
	routes: [
		{path: "/", component: App, props: false},
		{path: "/list", component: App, props: false},
		{path: "/list/:pCurrentListName", component: App, props: true}
	]
})

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
const sessionGuid = uuidv4();

const EventBus = new Vue()
Object.defineProperties(Vue.prototype, {
	$bus: {
		get: function () {
			return EventBus;
		}
	},
	$sessionGuid: {
		get: function () {
			return sessionGuid;
		}
	},
	$handleError: {
		get: function () {
			return function(error) {
				let text = "error ocurs"
				if (error != null && error.body != null) {
					text = error.body.message;
				} else if (error !== "") {
					text = error;
				}
				this.$bus.$emit('showAlert', text)
			}
		}
	},
	$showInfoToast: {
		get: function () {
			return function(text) {
				Vue.toasted.info(text, 
					{
						action: [
							{
						        text : 'Close',
						        onClick : (e, toastObject) => {
						            toastObject.goAway(100);
						        }
						    }
						],
						position: "bottom-right", 
	 					duration : 5000
	 				});
			}
		}
	},
	$showSuccessToast: {
		get: function () {
			return function(text) {
				Vue.toasted.success(text, 
					{
						action: [
							{
						        text : 'Close',
						        onClick : (e, toastObject) => {
						            toastObject.goAway(100);
						        }
						    }
						],
						position: "bottom-right", 
	 					duration : 5000
	 				});
			}
		}
	},
	$showErrorToast: {
		get: function () {
			return function(text) {
				Vue.toasted.error(text, 
					{
						action: [
							{
						        text : 'Close',
						        onClick : (e, toastObject) => {
						            toastObject.goAway(100);
						        }
						    }
						],
						position: "bottom-right", 
	 					duration : 5000
	 				});
			}
		}
	},
	$loadingStart: {
		get: function() {
			return function() {
				this.$bus.$emit('loadingStart');
			}
		}
	},
	$loadingEnd: {
		get: function() {
			return function() {
				this.$bus.$emit('loadingEnd');
			}
		}
	},
	$isEmpty: {
		get: function() {
			return function(some) {
				return (some === null) ||
					   (some === undefined) ||
					   (typeof some === "undefined") ||
					   (some === "")
					   ;
			}
		}
	}
})

const app = new Vue({
	template: "<router-view><App/></router-view>",
	components: {
		App
	},
	router
}).$mount('#app')